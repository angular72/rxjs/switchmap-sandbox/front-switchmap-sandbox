import { tap, switchMap, map, toArray, startWith, finalize } from 'rxjs/operators';
import { Product } from './../../switch-map/models/product.model';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class FakeHttpService {
    /* Ici, on simule une requête HTTP: une seule valeur est émise par la source: le payload de la requête puis
    suite à cela, la source passe à l'état "completed".
    */
    private _simulateHttpRequest$: Observable<Product[]> = of([
        new Product('1', 'Product 1'),
        new Product('2', 'Product 2'),
        new Product('3', 'Product 3'),
        new Product('4', 'Product 4'),
    ]);

    private _simulateSourceMultipleValue$: Observable<any> = of(
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
    );

    constructor() { }

    getOneByOne(): Observable<Product> {
        return this._simulateHttpRequest$
            .pipe(
                /* Ici, on aura dans les logs un tableau contenant nos 4 produits. En effet, une seule valeur est
                émise par la source qui est le tableau de nos 4 produits et donc du coup il n'y a dans les logs
                qu'une seule émission.
                */
                tap(console.log),
                /* Avec l'introduction de l'opérateur switchMap dans notre pipe, l'émission des données va être différente.
                Le but principal de l'opérateur switchMap est qu'à chaque fois que la source émet une valeur, l'opérateur se
                désabonne de l'ancienne valeur et s'abonne à la valeur émise la plus récente par la source. La valeur retournée
                par l'opérateur switchMap est wrappée dans une ObservedValueOf qui hérite de ObservableInput qui lui-même hérite
                du type Observable. En se référant à la documentation, on peut s'apercevoir qu'un objet de type ObservableInput
                possède une particularité: si on retourne un tableau (Array), alors le tableau va être interprété comme une
                Observable qui va émettre les valeurs de ce dernier une par une, de la gauche vers la droite. Lorsque toutes
                les valeurs du tableau sont émises, l'Observable est complétée. Au final, un tableau retourné par l'opérateur
                switchMap va agir comme une nouvelle source de données.
                */
                switchMap( ( products: Product[] ) => products),
                /* A cette étape là du pipe, le tableau de produits est émis un par un par l'ObservableInput. De ce fait, dans
                la console sont loggées les données une par une et non plus sous forme de tableau.
                */
                tap(console.log)
            );
    }

    getAll(): Observable<Product[]> {
        return this._simulateHttpRequest$
            .pipe(
                switchMap( ( products: Product[] ) => products),
                /* D'après les explications précédentes, il devient donc aisé de transformer chaque
                produit du tableau en appliquant l'opérateur map.
                */
                map( ( product: Product ) => {
                    product.name = `${product.name} mappé !`;
                    return product;
                }),
                /* Puisque les valeurs sont émises une à une (grâce à l'opérateur switchMap), on obtiendrait donc
                à la fin de l'émission une seule valeur qui serait le dernier produit de la liste (puisque le dernier
                émis par la source créée par ObservableInput). Or, nous voulons une liste de produit. C'est ici que l'opérateur
                toArray intervient. Cet opérateur va collecter toutes les données émises dans un tableau et émettre ce tableau
                uniquement quand la source sera complétée ce qui correspond parfaitement à notre besoin.
                */
                toArray(),
                tap(console.log)
            );
    }

    getAllNotOptimized(): Observable<Product[]> {
        return this._simulateHttpRequest$
            .pipe(
                /* Cette méthode contient une solution alternative à la solution proposée dans la méthode getAll(). Cependant,
                on s'aperçoit que cette solution est plus fastidieuse et moins claire. En revanche, on obtient le même résultat.
                */
                map( ( products: Product[] ) => {
                    products.map( ( product: Product ) => {
                        product.name = `${product.name} mappé (not optimized) !`;
                        return product;
                    })
                    return products;
                }),
                tap(console.log)
            );
    }

    sourceEmitMultipleValue(): Observable<number[]> {
        return this._simulateSourceMultipleValue$
            .pipe(
                /* Ici, on va obtenir dans les logs 3 tableaux qui correspondent aux 3 tableaux émis, un par un, par
                la source. Entre les 3 tableaux loggés dans la console, on obtient les valeurs du tableau une par une.
                Cela s'explique par le fait qu'à chaque fois qu'une valeur est émise par la source, tout le pipe et ses
                opérateurs sont executés avant de passer à la prochaine valeur émise par la source.
                */
                tap(console.log),
                switchMap( ( numbers: number[] ) => numbers),
                /* Ici, on va obtenir chaque valeur de chaque tableau une à une dans la console
                */
                tap(console.log),
                toArray(),
                /* Puisque l'opérateur toArray stocke toutes les valeurs émises par la source dans un tableau et les émets
                quand la source passe à l'état "complete", nous obtenons dans la console un tableau qui contient toutes les
                valeurs émises depuis le début jusqu'à la dernière valeur valeur (valeur retournée: [1, 2, 3, 4, 5, 6, 7, 8, 9])
                */
                tap(console.log),
                /* Remarquons la puissance d'un pipe et des opérateurs: on peut, à partir du tableau obtenu, de nouveau appliquer
                l'opérateur switchMap puis ensuite l'opérateur map pour, à nouveau, transformer chaque valeur une à une avant de
                retourner un nouveau tableau final.
                */
                switchMap ( ( numbers: number[] ) => numbers),
                map( ( num: number ) => ++num),
                toArray(),
                tap(console.log)
        );
    }

    startWith(): Observable<Product[]> {
        return this._simulateHttpRequest$
        .pipe(
            /* Cet opérateur permet d'émettre une toute première valeur (utile lorsque l'on veut par exemple afficher des
            barres de chargement, faire patienter l'user, ou éviter des erreurs => on initialise le state par exemple avec une
            valeur par défaut.). En allant regarder dans la console, on s'aperçoit du coup qu'avant de logger la liste des produits,
            un tableau vide est loggé.
            */
            startWith([]),
            /* Permet de réaliser une action quand la source est completée (par exemple afficher une notification, signaler à
            un composant que les données ont été chargées etc)
            */
            finalize( () => console.log('Terminé !'))
        );
    }
}
