import { SimulateHttpRequestComponent } from './components/simulate-http-request.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SimulateHttpRequestRoutingModule } from './simulate-http-request-routing.module';


@NgModule({
  declarations: [
      SimulateHttpRequestComponent
  ],
  imports: [
    CommonModule,
    SimulateHttpRequestRoutingModule
  ]
})
export class SimulateHttpRequestModule { }
