import { FakeHttpService } from './../services/fake-http.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-simulate-http-request',
    templateUrl: './simulate-http-request.component.html',
    styleUrls: ['./simulate-http-request.component.scss']
})
export class SimulateHttpRequestComponent implements OnInit {

    constructor(private _fakeHttpService: FakeHttpService) { }

    ngOnInit(): void {
        console.log('');
        console.log('');

        console.log('One by one');
        this._fakeHttpService.getOneByOne().subscribe();

        console.log('');
        console.log('');

        console.log('get all');
        this._fakeHttpService.getAll().subscribe();

        console.log('');
        console.log('');

        console.log('get all (not optimized)');
        this._fakeHttpService.getAllNotOptimized().subscribe();

        console.log('');
        console.log('');

        console.log('Source which emit multiple value')
        this._fakeHttpService.sourceEmitMultipleValue().subscribe();

        console.log('');
        console.log('');

        console.log('start with operator')
        this._fakeHttpService.startWith().subscribe(console.log);
    }

}
