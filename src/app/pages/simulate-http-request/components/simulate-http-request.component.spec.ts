import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimulateHttpRequestComponent } from './simulate-http-request.component';

describe('SimulateHttpRequestComponent', () => {
  let component: SimulateHttpRequestComponent;
  let fixture: ComponentFixture<SimulateHttpRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimulateHttpRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimulateHttpRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
