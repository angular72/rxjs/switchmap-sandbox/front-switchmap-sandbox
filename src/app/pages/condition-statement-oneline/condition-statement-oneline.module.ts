import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConditionStatementOnelineRoutingModule } from './condition-statement-oneline-routing.module';
import { ConditionStatementOnelineComponent } from './components/condition-statement-oneline.component';


@NgModule({
  declarations: [ConditionStatementOnelineComponent],
  imports: [
    CommonModule,
    ConditionStatementOnelineRoutingModule
  ]
})
export class ConditionStatementOnelineModule { }
