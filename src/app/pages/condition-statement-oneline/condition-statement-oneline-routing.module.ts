import { ConditionStatementOnelineComponent } from './components/condition-statement-oneline.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '', component: ConditionStatementOnelineComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConditionStatementOnelineRoutingModule { }
