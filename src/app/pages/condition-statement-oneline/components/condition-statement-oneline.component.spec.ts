import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConditionStatementOnelineComponent } from './condition-statement-oneline.component';

describe('ConditionStatementOnelineComponent', () => {
  let component: ConditionStatementOnelineComponent;
  let fixture: ComponentFixture<ConditionStatementOnelineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConditionStatementOnelineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConditionStatementOnelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
