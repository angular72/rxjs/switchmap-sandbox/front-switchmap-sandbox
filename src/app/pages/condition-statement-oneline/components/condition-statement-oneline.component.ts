import { Product } from './../../switch-map/models/product.model';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-condition-statement-oneline',
    templateUrl: './condition-statement-oneline.component.html',
    styleUrls: ['./condition-statement-oneline.component.scss']
})
export class ConditionStatementOnelineComponent implements OnInit {

    constructor() { }

    ngOnInit(): void {
        // ------ Solution classique
        const array1 = [
            ['a', 'b'],
            null,
            ['c', 'd'],
        ];

        console.log('Solution classique');
        array1.map( ( value: string[] | null ) => {
            if (value) {
                value.push('z');
            }
        });
        console.log(array1);

        console.log('');
        console.log('');


        // ------ Solution intermédiaire
        const array2 = [
            ['a', 'b'],
            null,
            ['c', 'd'],
        ];

        console.log('Solution intermédiaire');
        // solution intermédiare avec une opération ternaire
        array2.map( ( value: string[] | null ) => value ? value.push('z') : null);
        console.log(array2);

        console.log('');
        console.log('');



        // ------ Solution optimisée
        const array3 = [
            ['a', 'b'],
            null,
            ['c', 'd'],
        ];
        console.log('Solution optimisée');
        // Raccourci: on peut mettre la condition suivi du statement. Le statement ne sera exécuté
        // que si la première condition est remplie. Raccourci syntaxique !
        // On voit dans la console que le tableau setté à null est ignoré
        array3.map( ( value: string[] | null ) => value && value.push('z'));
        console.log(array3);
    }

}
