import { Product } from './../models/product.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, map, startWith, switchMap, tap, toArray } from 'rxjs/operators';

@Injectable()
export class ProductService {
    private BASE_URL_PRODUCT_SERVICE = environment.BASE_URL_PRODUCT_SERVICE;

    constructor(private _http: HttpClient) { }

    public getAll(): Observable<Product[]> {
        return this._http.get<Product[]>(`${this.BASE_URL_PRODUCT_SERVICE}/products`)
            .pipe(
                startWith([]),
                catchError( ( error: any) => of(error)),
                switchMap( ( products: Product[] ) => products),
                tap(
                    this.log,
                    this.logError,
                    () => this.log('TERMINATE SWITCHMAP !')),
                map(this.fromJSONToProduct),
                tap(
                    this.log,
                    this.logError,
                    () => this.log('TERMINATE MAP!')),
                toArray(),
                tap(
                    this.log,
                    this.logError,
                    () => this.log('TERMINATE TOARRAY!'))
            );
    }

    private log<T>(toLog: T): void {
        console.log(toLog);
    }

    private logError<T>(toLog: T): void {
        console.error(toLog);
    }

    private fromJSONToProduct(product: Product): Product {
        return new Product(product.uuid, product.name);
    }
}
