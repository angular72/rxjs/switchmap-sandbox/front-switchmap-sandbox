import { ProductService } from '../services/product.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/product.model';

@Component({
    selector: 'app-switch-map',
    templateUrl: './switch-map.component.html',
    styleUrls: ['./switch-map.component.scss']
})
export class SwitchMapComponent implements OnInit {
    products$?: Observable<Product[]>;


    constructor(private _productService: ProductService) { }

    ngOnInit(): void {
        this.products$ = this._productService.getAll();
    }
}
