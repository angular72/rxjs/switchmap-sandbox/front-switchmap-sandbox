import { ProductService } from './services/product.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SwitchMapRoutingModule } from './switch-map-routing.module';
import { SwitchMapComponent } from './components/switch-map.component';


@NgModule({
  declarations: [SwitchMapComponent],
  imports: [
    CommonModule,
    SwitchMapRoutingModule
  ],
  providers: [
    ProductService
  ]
})
export class SwitchMapModule { }
