import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: 'switch-map',
        loadChildren: () => import('./pages/switch-map/switch-map.module').then(m => m.SwitchMapModule)
    },
    {
        path: 'simulate-http-request',
        loadChildren: () =>
            import('./pages/simulate-http-request/simulate-http-request-routing.module')
                .then(m => m.SimulateHttpRequestRoutingModule)
    },
    {
        path: 'condition-statement-oneline',
        loadChildren: () =>
        import('./pages/condition-statement-oneline/condition-statement-oneline.module')
                .then( m => m.ConditionStatementOnelineModule)
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
